from ._cf_build_state import ControlFileBuildState
from ._cf_load_factory import ControlFileLoadMixin
from ..dataclasses.scope import ScopeList
from ..dataclasses.types import PathLike
from ..utils.settings import Settings
from ..abc.build_state import BuildState


class TESF(ControlFileLoadMixin, ControlFileBuildState):

    def __new__(cls,
                path: PathLike = None,
                settings: Settings = None,
                parent: BuildState = None,
                scope: ScopeList = None,
                **kwargs) -> 'TESF':
        """Override __new__ to make sure a TEF class is returned."""
        return object.__new__(cls)
    
    def __init__(self,
                path: PathLike = None,
                settings: Settings = None,
                parent: BuildState = None,
                scope: ScopeList = None,
                **kwargs) -> 'TESF':
        super(TESF, self).__init__(path, settings, parent, scope, **kwargs)

    def __repr__(self):
        return f'<TuflowExternalStressFile> {str(self)}'
