from ._cf_build_state import ControlFileBuildState
from ._cf_load_factory import ControlFileLoadMixin
from ..dataclasses.scope import ScopeList
from ..dataclasses.types import PathLike
from ..utils.settings import Settings
from ..abc.build_state import BuildState


class TOC(ControlFileLoadMixin, ControlFileBuildState):

    def __new__(cls,
                path: PathLike = None,
                settings: Settings = None,
                parent: BuildState = None,
                scope: ScopeList = None,
                **kwargs) -> 'TOC':
        """Override __new__ to make sure a TOC class is returned."""
        return object.__new__(cls)
    
    def __init__(self,
                path: PathLike = None,
                settings: Settings = None,
                parent: BuildState = None,
                scope: ScopeList = None,
                **kwargs) -> 'TOC':
        super(TOC, self).__init__(path, settings, parent, scope, **kwargs)

    def __repr__(self):
        return f'<TuflowOperatingControl> {str(self)}'