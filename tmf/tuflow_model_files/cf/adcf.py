from ._cf_build_state import ControlFileBuildState
from ._cf_load_factory import ControlFileLoadMixin
from ..dataclasses.scope import ScopeList
from ..inp._inp_build_state import InputBuildState
from ..dataclasses.types import PathLike
from ..utils.settings import Settings
from ..abc.build_state import BuildState


class ADCF(ControlFileLoadMixin, ControlFileBuildState):

    def __new__(cls,
                path: PathLike = None,
                settings: Settings = None,
                parent: BuildState = None,
                scope: ScopeList = None,
                **kwargs) -> 'ADCF':
        """Override __new__ to make sure a ADCF class is returned."""
        return object.__new__(cls)
    
    def __init__(self,
                path: PathLike = None,
                settings: Settings = None,
                parent: BuildState = None,
                scope: ScopeList = None,
                **kwargs) -> 'ADCF':
        super(ADCF, self).__init__(path, settings, parent, scope, **kwargs)

    def __repr__(self):
        return f'<ADControlFile> {str(self)}'
