from ._cf_build_state import ControlFileBuildState
from ._cf_load_factory import ControlFileLoadMixin
from ..dataclasses.inputs import Inputs
from ..dataclasses.scope import ScopeList
from ..dataclasses.types import PathLike
from ..dataclasses.file import TuflowPath
from ..utils.settings import Settings
from ..abc.build_state import BuildState


class TRD(ControlFileLoadMixin, ControlFileBuildState):

    def __new__(cls,
                path: PathLike = None,
                settings: Settings = None,
                parent: BuildState = None,
                scope: ScopeList = None,
                **kwargs) -> 'TRD':
        """Override __new__ to make sure a TRD class is returned."""
        return object.__new__(cls)
    
    def __init__(self,
                path: PathLike = None,
                settings: Settings = None,
                parent: BuildState = None,
                scope: ScopeList = None,
                **kwargs) -> 'TRD':
        super(TRD, self).__init__(path, settings, parent, scope, **kwargs)

    def __repr__(self):
        return f'<TuflowReadFile> {str(self)}'

    @staticmethod
    def get_inputs(cf: ControlFileBuildState, trd_path: PathLike) -> Inputs:
        trd = TuflowPath(trd_path)
        return Inputs([x for x in cf.inputs if x.trd == trd])
