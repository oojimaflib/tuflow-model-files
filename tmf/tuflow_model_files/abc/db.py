from abc import abstractmethod
from typing import Union

import pandas as pd

from ..dataclasses.inputs import Inputs
from ..dataclasses.types import PathLike


class Database:

    def __getitem__(self, item):
        if self._df is not None:
            try:
                i = [x.lower() for x in self._df.index].index(item.lower())
            except Exception as e:
                i = -1
            if i == -1:
                raise KeyError('Item {0} not found in database'.format(item))
            return self._df.iloc[i]

    def __contains__(self, item):
        if self._df is not None:
            if isinstance(item, str):
                return item.lower() in [x.lower() for x in self._df.index]
            else:
                return item in self._df.index
        return False

    def db(self) -> pd.DataFrame:
        """Returns the database as a DataFrame."""
        return self._df

    def index_to_file(self, index: Union[str, int]) -> list[PathLike]:
        """
        Returns files associated with the given database index/key.

        :param index:
            str | int - The index/key to get the associated files for.
        """
        return self._index_to_file.get(index, [])

    def gis_inputs(self, recursive: bool = True) -> Inputs:
        """Here so that parent classes can call these on all child classes without type checking."""
        return Inputs([])

    def grid_inputs(self, recursive: bool = True) -> Inputs:
        """Here so that parent classes can call these on all child classes without type checking."""
        return Inputs([])

    def tin_inputs(self, recursive: bool = True) -> Inputs:
        """Here so that parent classes can call these on all child classes without type checking."""
        return Inputs([])

    def get_inputs(self, recursive: bool = True) -> Inputs:
        """Here so that parent classes can call these on all child classes without type checking."""
        return Inputs([])

    @abstractmethod
    def value(self, item: str, *args, **kwargs) -> list[any]:...
