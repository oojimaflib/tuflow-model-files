import re
import typing
from typing import TYPE_CHECKING, Union
from uuid import UUID

from ..dataclasses.event import Event, EventDatabase
from ..dataclasses.scope import Scope
from ..dataclasses.types import SearchTagLike
from ..utils.commands import EventCommand
from ..utils.context import Context
from ..dataclasses.file import TuflowPath
from ..dataclasses.inputs import Inputs

if TYPE_CHECKING:
    from ..dataclasses.inputs import Inputs
    from ..abc.input import Input
    from ..abc.db import Database
    from ..cf._cf_build_state import ControlFileBuildState


class ControlFile:

    def __setattr__(self, key, value, private: bool = False, gate: int = 0):
        """
        Override this method to prevent users from changing property values.
        This is required as control file inputs are assigned as properties to the control file class, and
        we don't want users thinking that modifying these will change the control file inputs. Methods such as
        "update_value", "append_input", "insert_input", "remove_input" - should be used instead.

        This only prevents users from changing properties of the control file class if the property has already
        been set by the load method and added to the "_priv_prop" dictionary. If the property needs to actually be
        updated, just pass in a secret 'gate' value that is not equal to zero.
        """
        if hasattr(self, '_priv_prop') and key in self._priv_prop and not gate:
            raise AttributeError(f'Cannot set attribute {key} on {self.__class__.__name__}')
        super().__setattr__(key, value)
        if private and hasattr(self, '_priv_prop'):
            self._priv_prop[key] = True

    @property
    def tcf(self):
        if not self.parent:
            return self
        else:
            tcf = self.parent
            while tcf.parent:
                tcf = tcf.parent
            return tcf

    def tgc(self, context: Context = None) -> 'ControlFile':
        """
        Returns the TGC ControlFile object (only applicable from the TCF class).

        If more than one TGC control file object exists, a Context object must be provided to resolve to the correct
        TGC.

        :param context:
            Context - a context object to resolve the correct TGC control file object. Not required unless more than one
            TGC control file object exists.
        """
        return self._find_control_file('geometry control file', context)

    def tbc(self, context: Context = None) -> 'ControlFile':
        """
        Returns the TBC ControlFile object (only applicable from the TCF class).

        If more than one TBC control file object exists, a Context object must be provided to resolve to the correct
        TBC.

        :param context:
            Context - a context object to resolve the correct TBC control file object. Not required unless more than one
            TBC control file object exists.
        """
        return self._find_control_file('bc control file', context)

    def ecf(self, context: Context = None) -> 'ControlFile':
        """
        Returns the ECF ControlFile object (only applicable from the TCF class).

        If more than one ECF control file object exists, a Context object must be provided to resolve to the correct
        ECF.

        :param context:
            Context - a context object to resolve the correct Estry control file object. Not required unless more than one
            Estry control file object exists.
        """
        return self._find_control_file('estry control file', context)

    def tscf(self, context: Context = None) -> 'ControlFile':
        """
        Returns the TSCF ControlFile object (only applicable from the TCF class).

        If more than one TSCF control file object exists, a Context object must be provided to resolve to the correct
        TSCF.

        :param context:
            Context - a context object to resolve the correct TSCF control file object. Not required unless more than one
            TSCF control file object exists.
        """
        return self._find_control_file('swmm control file', context)

    def bc_dbase(self, context: Context = None) -> 'ControlFile':
        """
        Returns the BcDatabase Database object (only applicable from the TCF class).

        If more than one BcDatabase object exists, a Context object must be provided to resolve to the correct
        BcDatabase.

        :param context:
            Context - a context object to resolve the correct BcDatabase object. Not required unless more than one
            BcDatabase object exists.
        """
        return self._find_control_file('bc database', context)

    def mat_file(self, context: Context = None) -> 'ControlFile':
        """
        Returns the Materials Database object (only applicable from the TCF class).

        If more than one Materials Database object exists, a Context object must be provided to resolve to the correct
        Materials Database.

        :param context:
            Context - a context object to resolve the correct Materials Database object. Not required unless more than one
            Materials Database object exists.
        """
        return self._find_control_file('read materials? file', context, regex=True)

    def tef(self, context: Context = None) -> 'ControlFile':
        """
        Returns the TEF ControlFile object (only applicable from the TCF class).

        If more than one TEF control file object exists, a Context object must be provided to resolve to the correct
        TEF.

        :param context:
            Context - a context object to resolve the correct TEF control file object. Not required unless more than one
            TEF control file object exists.
        """
        return self._find_control_file('event file', context)

    def event_database(self, context: Context = None) -> EventDatabase:
        """
        Returns the EventDatabase object (only applicable from the TCF class).

        If more than one EventDatabase object exists, a Context object must be provided to resolve to the correct
        EventDatabase.

        :param context:
            Context - a context object to resolve the correct EventDatabase object. Not required unless more than one
            EventDatabase object exists.
        """
        tef = self._find_control_file('event file', context)
        if tef is None:
            return EventDatabase()
        return self._event_cf_to_db(tef)

    def output_folder_1d(self, context: Context = None) -> TuflowPath:
        """
        Returns the 1D output folder.

        Returns the last instance of the command. If more than one Output Folder exists and some exist in
        IF logic blocks an exception will be raised.

        :param context:
            Context - a context object to resolve the correct output folder. Not required unless more than one
            output folder exists.
        """
        output_folders = []
        inputs = self.find_input(command='output folder', recursive=True)
        for inp in inputs:
            if '1D' in inp.command.upper():
                output_folders.append(inp)
            if hasattr(self, 'scope') and Scope('1D Domain') in inp.scope():
                output_folders.append(inp)
            if hasattr(self, '_bs') and Scope('1D Domain') in inp._bs.scope():
                output_folders.append(inp)
            if '<EstryControlFile>' in repr(inp.parent) or '<ECFContext>' in repr(inp.parent):
                output_folders.append(inp)

        if len(output_folders) > 1 and hasattr(self, 'scope') and [x for x in output_folders if Scope('GLOBAL') not in x.scope()]:
            if not context:
                raise ValueError('{0} requires context to resolve'.format('Output Folder'))
            else:  # context has been provided, can try and resolve
                for i, inp in enumerate(output_folders[:]):
                    if context.in_context_by_scope(inp._scope):
                        output_folders[i] = inp

        if output_folders:
            return TuflowPath(output_folders[-1].expanded_value)
        else:
            return self._path.parent

    def output_folder_2d(self, context: Context = None) -> TuflowPath:
        """
        Returns the 2D output folder.

        Returns the last instance of the command. If more than one Output Folder exists and some exist in
        IF logic blocks an exception will be raised.

        :param context:
            Context - a context object to resolve the correct output folder. Not required unless more than one
            output folder exists.
        """
        output_folders = []
        inputs = self.find_input(command='output folder', recursive=True)
        for inp in inputs:
            if '1D' in inp.command.upper():
                continue
            if hasattr(self, 'scope') and Scope('1D Domain') in inp.scope():
                continue
            if hasattr(self, '_bs') and Scope('1D Domain') in inp._bs.scope():
                continue
            if '<EstryControlFile>' in repr(inp.parent) or '<ECFContext>' in repr(inp.parent):
                continue
            if '<TuflowSWMMControl>' in repr(inp.parent) or '<TSCFContext>' in repr(inp.parent):
                continue
            output_folders.append(inp)

        if len(output_folders) > 1 and hasattr(self, 'scope') and [x for x in output_folders if Scope('GLOBAL') not in x.scope()]:
            if not context:
                raise ValueError('{0} requires context to resolve'.format('Output Folder'))
            else:  # context has been provided, can try and resolve
                for i, inp in enumerate(output_folders[:]):
                    if context.in_context_by_scope(inp._scope):
                        output_folders[i] = inp

        if output_folders:
            return TuflowPath(output_folders[-1].expanded_value)

    def commands(self) -> list[str]:
        """
        Returns a list of all the commands in the current control file.

        ES Note:
            Probably can remove this, not sure what it offers over just looking at the list of inputs.
        """
        return [input.command for input in self.inputs if input]

    def input(self, uuid: Union[str, UUID]) -> 'Input':
        """
        Returns the input with the given UUID.

        :param uuid:
            str - the UUID of the input to return.
        """
        if isinstance(uuid, str):
            uuid = UUID(uuid)
        for inp in self.inputs:
            if inp.uuid == uuid:
                return inp
            if 'ControlFileInput' in repr(inp):
                loaded_value = self.input_to_loaded_value(inp)
                if loaded_value is None:
                    continue
                if isinstance(loaded_value, Inputs):
                    for inp_ in loaded_value:
                        inp2 = inp_.input(uuid)
                        if inp2:
                            return inp2
                else:
                    inp_ = loaded_value.input(uuid)
                    if inp_:
                        return inp_

    def find_input(self, filter: str = None, command: str = None,
                   value: str = None, recursive: bool = True, regex: bool = False, regex_flags: int = 0,
                   tags: SearchTagLike = (), callback: typing.Callable = None) -> 'Inputs':
        """
        Find a particular input(s) by using a filter. The filter can be for the entire input, or localised to the
        command or value side of the input. The filter can be a string or a regular expression.

        The parameters are not mutually exclusive, so if a filter can be provided for the command and the
        value parameters.

        :param filter:
            str - a string or regular expression to filter the input by.
                  This will search through the entire input string (not comments).
        :param command:
            str - a string or regular expression to filter the input by.
                  This will search through the command side of the input (i.e. LHS).
        :param value:
            str - a string or regular expression to filter the input by.
                  This will search through the value side of the input (i.e. RHS).
        :param regex:
            bool - if set to True, the filter, command, and value parameters will be treated as regular expressions.
        :param tags:
            SearchTagLike - a list of tags to filter the input by. This can be a string (single tag)
                            or list/tuple of strings that will be used to filter the input by tag keys which
                            correspond to properties contained in the input. Tags themselves can be tuples with a
                            value to compare against (key, value) otherwise the value will be assumed as True.
        :param callback:
            Callable - a function that will be called with the input as an argument.
        """
        from ..dataclasses.inputs import Inputs

        inputs = Inputs()
        for input_ in self.inputs:
            if input_.is_match(filter, command, value, regex, regex_flags, tags, callback):
                inputs.append(input_)
            if recursive and 'ControlFileInput' in repr(input_):
                loaded_value = self.input_to_loaded_value(input_)
                if loaded_value is None:
                    continue
                if isinstance(loaded_value, Inputs):
                    for inp in loaded_value:
                        inputs.extend(inp.find_input(filter, command, value, recursive, regex, regex_flags, tags, callback))
                else:
                    inputs.extend(loaded_value.find_input(filter, command, value, recursive, regex, regex_flags, tags, callback))

        return inputs

    def gis_inputs(self, recursive: bool = True) -> 'Inputs':
        """
        Returns all GIS inputs in the control file. If recursive is set to True, will also search through any
        child control files.

        :param recursive:
            bool - if set to True, will also search through any child control files.
        """
        from ..dataclasses.inputs import Inputs
        inputs = Inputs()
        for input_ in self.inputs:
            if input_.raw_command_obj().is_read_gis() or input_.raw_command_obj().is_read_projection():
                inputs.append(input_)
            loaded_value = self.input_to_loaded_value(input_)
            if loaded_value and recursive:
                if isinstance(loaded_value, Inputs):
                    for inp in loaded_value:
                        inputs.extend(inp.gis_inputs(recursive))
                else:
                    inputs.extend(loaded_value.gis_inputs(recursive))

        return inputs

    def grid_inputs(self, recursive: bool = True) -> 'Inputs':
        """
        Returns all grid inputs in the control file. If recursive is set to True, will also search through any
        child control files.

        :param recursive:
            bool - if set to True, will also search through any child control files.
        """
        from ..dataclasses.inputs import Inputs, ComplexInputs
        inputs = Inputs()
        for input_ in self.inputs:
            if input_.raw_command_obj().is_read_grid():
                inputs.append(input_)
            loaded_value = self.input_to_loaded_value(input_)
            if loaded_value and recursive:
                if isinstance(loaded_value, Inputs):
                    for inp in loaded_value:
                        inputs.extend(inp.grid_inputs(recursive))
                else:
                    inputs.extend(loaded_value.grid_inputs(recursive))

        return inputs

    def tin_inputs(self, recursive: bool = True) -> 'Inputs':
        """
        Returns all TIN inputs in the control file. If recursive is set to True, will also search through any
        child control files.

        :param recursive:
            bool - if set to True, will also search through any child control files.
        """
        from ..dataclasses.inputs import Inputs, ComplexInputs
        inputs = Inputs()
        for input_ in self.inputs:
            if input_.raw_command_obj().is_read_tin():
                inputs.append(input_)
            loaded_value = self.input_to_loaded_value(input_)
            if loaded_value and recursive:
                if isinstance(loaded_value, Inputs):
                    for inp in loaded_value:
                        inputs.extend(inp.tin_inputs(recursive))
                else:
                    inputs.extend(loaded_value.tin_inputs(recursive))

        return inputs

    def get_inputs(self, recursive: bool = True) -> 'Inputs':
        """
        Returns all inputs in the control file. If recursive is set to True, will also search through any
        child control files.

        :param recursive:
            bool - if set to True, will also search through any child control files.
        """
        from ..dataclasses.inputs import Inputs, ComplexInputs
        inputs = Inputs()
        for input_ in self.inputs:
            if input_ not in inputs:
                inputs.append(input_)
            loaded_value = self.input_to_loaded_value(input_)
            if loaded_value and recursive:
                if isinstance(loaded_value, Inputs):
                    for inp in loaded_value:
                        inputs.extend(inp.get_inputs(recursive))
                else:
                    inputs.extend(loaded_value.get_inputs(recursive))

        return inputs

    def input_to_loaded_value(self, inp: 'Input') -> any:
        """
        Returns the loaded class object for a given input.

        e.g. Given the input specifying the geometry control file, this method will return the TGC class object
        loaded from that given input.

        Currently only applicable for ControlFile objects and Database objects, otherwise will return None.

        :param inp:
            Input - the input to get the loaded class object for.
        """
        return self._input_to_loaded_value.get(inp)

    def _input_as_attr(self, inp: 'Input') -> None:
        if not inp:
            return
        attr = re.sub(r'\s+', '_', inp.command.lower())
        attr = re.sub(r'\(.*\)', '', attr)
        attr = attr.rstrip('_')
        if hasattr(self, attr) and not isinstance(getattr(self, attr), Inputs):
            value = Inputs()
            value.append(getattr(self, attr))
            if inp.raw_command_obj().is_control_file() or inp.raw_command_obj().is_read_database():
                value.extend(self.input_to_loaded_value(inp))
            else:
                value.append(inp.value)
            self.__setattr__(attr, value, True, 1)
        elif not hasattr(self, attr):
            if inp.raw_command_obj().is_control_file() or inp.raw_command_obj().is_read_database():
                loaded_value = self.input_to_loaded_value(inp)
                if hasattr(loaded_value, '__len__') and len(loaded_value) == 1:
                    loaded_value = loaded_value[0]
                self.__setattr__(attr, loaded_value, True, 1)
            else:
                self.__setattr__(attr, inp.value, True, 1)
        else:
            if inp.raw_command_obj().is_control_file() or inp.raw_command_obj().is_read_database():
                getattr(self, attr).extend(self.input_to_loaded_value(inp))
            else:
                getattr(self, attr).append(inp.value)

    def _replace_attr(self, inp: 'Input', old_value: any) -> None:
        if not inp:
            return
        attr = re.sub(r'\s+', '_', inp.command.lower())
        attr = re.sub(r'\(.*\)', '', attr)
        attr = attr.rstrip('_')
        has_attr = hasattr(self, attr)
        if has_attr and isinstance(getattr(self, attr), Inputs):
            values = getattr(self, attr)
            if old_value in values:
                i = values.index(old_value)
                if inp.raw_command_obj().is_control_file() or inp.raw_command_obj().is_read_database():
                    values[i] = self.input_to_loaded_value(inp)
                else:
                    values[i] = inp.value
        else:
            if inp.raw_command_obj().is_control_file() or inp.raw_command_obj().is_read_database():
                self.__setattr__(attr, self.input_to_loaded_value(inp), True, 1)
            else:
                self.__setattr__(attr, inp.value, True, 1)

    def _remove_attr(self, inp: 'Input', old_value: any) -> None:
        if not inp:
            return
        attr = re.sub(r'\s+', '_', inp.command.lower())
        attr = re.sub(r'\(.*\)', '', attr)
        attr = attr.rstrip('_')
        has_attr = hasattr(self, attr)
        if has_attr and isinstance(getattr(self, attr), Inputs):
            values = getattr(self, attr)
            if old_value in values:
                values.remove(old_value)
                if len(values) == 1:
                    self.__setattr__(attr, values[0], True, 1)
        elif has_attr:
            delattr(self, attr)

    def _event_cf_to_db(self, event_cf: 'ControlFileBuildState') -> EventDatabase:
        events = EventDatabase()
        if event_cf is None:
            return events
        for input_ in event_cf.inputs:
            cmd = EventCommand(input_.raw_command_obj().original_text, input_.raw_command_obj().settings)
            if not cmd.is_event_source():
                continue
            var, val = cmd.get_event_source()
            name_ = [x for x in input_._scope if x == Scope('EVENT VARIABLE')][0].name
            if isinstance(name_, list):
                name_ = name_[0]
            events[name_] = Event(name_, var, val)

        return events

    def _find_control_file(self, command: str, context: Context = None, regex: bool = False) -> Union['ControlFile', 'Database']:
        if command.lower() != 'read file' and 'TuflowControlFile' not in repr(self) and 'TCF' not in repr(self):
            raise NotImplementedError('Control file command only possible from TCF class')
        inputs = self.find_input(command=command, regex=regex)
        if len(inputs) > 1 or (hasattr(self, '_scope') and inputs and Scope('GLOBAL') not in inputs[0]._scope):
            if context is None:
                raise ValueError('{0} requires context to resolve'.format(command))
            else:
                input_ = None
                for inp in inputs:
                    if context.in_context_by_scope(inp._scope):
                        if input_ is not None:
                            raise ValueError('Multiple commands found in context')
                        input_ = inp
        elif inputs:
            input_ = inputs[0]
        else:
            input_ = None

        if input_ is None:
            return input_

        loaded_value = self.input_to_loaded_value(input_)
        if isinstance(loaded_value, Inputs):
            if len(loaded_value) > 1:
                if context is None:
                    raise ValueError('{0} requires context to resolve'.format(command))
                value_tr = context.translate(input_.expanded_value)
                if value_tr in [x._path for x in loaded_value]:
                    value = loaded_value[[x._path for x in loaded_value].index(value_tr)]
                else:
                    value = None
            value = loaded_value[0]
        elif not loaded_value:
            value = None
        else:
            value = loaded_value

        return value
