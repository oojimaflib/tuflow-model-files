from pathlib import Path
from typing import Union

from ...dataclasses.types import PathLike

from ...utils import logging as tmf_logging
logger = tmf_logging.get_tmf_logger()


class DatabaseDriver:

    __slots__ = ('_path')

    def __new__(cls, *args, **kwargs):
        from .csv import CsvDatabaseDriver

        if not args or (not isinstance(args[0], str) and not isinstance(args[0], Path)):
            logger.error('DatabaseDriver must be initialised with a str or Path object')
            raise ValueError('DatabaseDriver must be initialised with a str or Path object')
        else:
            path = args[0]

        # csv database
        self = object.__new__(CsvDatabaseDriver)
        if self._test(path):
            return self

        logger.error('Drive initialisation failed: could not determine database type')
        raise ValueError('Could not determine database type')

    def _test(self, path: PathLike) -> bool:
        logger.error('_test method must be implemented by driver class')
        raise NotImplementedError

    def name(self) -> str:
        logger.error('name method must be implemented by driver class')
        raise NotImplementedError

    def load(self, path: PathLike, header: int, index_col: Union[int, bool]):
        logger.error('load method must be implemented by driver class')
        raise NotImplementedError
