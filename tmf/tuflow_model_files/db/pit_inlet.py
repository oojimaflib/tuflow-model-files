import pandas as pd

from ._db_build_state import DatabaseBuildState
from ..dataclasses.types import PathLike
from ..dataclasses.scope import ScopeList

from ..utils import logging as tmf_logging
logger = tmf_logging.get_tmf_logger()


class PitInletDatabase(DatabaseBuildState):
    """Database class for pit inlet properties."""

    __slots__ = ('_source_index', '_header_index', '_index_col')

    def __init__(self, path: PathLike = None, scope: ScopeList = None, var_names: list[str] = ()) -> None:
        self._source_index = 0
        self._header_index = 0
        self._index_col = 0
        super().__init__(path, scope, var_names)

    @staticmethod
    def get_value(db_path: PathLike, df: pd.DataFrame, index: str) -> any:
        pass
