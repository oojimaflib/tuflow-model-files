from .file import FileInput


class AttrInput(FileInput):
    """
    Class for handling attribute files references within a vector file.
    """

    def __repr__(self):
        return f'<AttrInput> {str(self)}'




