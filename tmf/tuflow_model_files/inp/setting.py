from ._inp_build_state import InputBuildState


class SettingInput(InputBuildState):
    """
    Input for Settings or Set commands.

    e.g.
    TUTORIAL Model == ON
    Set Code == 0
    """

    def __repr__(self):
        return f'<SettingInput> {str(self)}'
