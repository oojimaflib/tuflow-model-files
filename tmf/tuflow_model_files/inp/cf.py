from .file import FileInput


class ControlFileInput(FileInput):
    """
    Class for control files.
    Includes TRD, TEF
    e.g.
    Geometry Control File == geometry_control_file.tgc
    """

    def __repr__(self):
        return f'<ControlFileInput> {str(self)}'
