from ._inp_build_state import InputBuildState


class CommentInput(InputBuildState):
    """
    Input class for comment only lines.

    e.g.
    ! Time Setting Inputs
    """

    def __repr__(self):
        return f'<CommentInput> {str(self)}'

    def __str__(self):
        return self._input.original_text

    def __bool__(self):
        return False
