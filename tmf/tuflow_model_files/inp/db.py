from .file import FileInput


class DatabaseInput(FileInput):
    """
    Input class for database inputs.
    e.g.
    BC Database == bc_database.csv
    Read Materials File == materials.csv
    """

    def __repr__(self):
        return f'<DatabaseInput> {str(self)}'
