import re
from typing import Union

from .patterns import replace_exact_names
from ..dataclasses.scope import Scope, ScopeList, VariableScope
from ..dataclasses.file import TuflowPath
from ..dataclasses.types import ContextLike, VariableMap
from ..dataclasses.event import EventDatabase

from ..utils import logging as tmf_logging
logger = tmf_logging.get_tmf_logger()


class Context:
    """
    Class for handling scenario/event/variable context for a given TUFLOW run state.

    Context can be initialised with
     - a list of arguments in the form of a TUFLOW batch file e.g. ['s1', 'EXG', 's2', '5m', '-e1', '100y']
     - a dictionary of arguments e.g. {'s1': 'EXG', 's2': '5m', 'e1': '100y'}
     - a list of scopes e.g. [Scope('SCENARIO', 'EXG'), Scope('SCENARIO', '5m'), Scope('EVENT', '100y')]

    The list of arguments can be unordered, however in most cases it is preferable to use an ordered list. Only in
    a small scope of tasks can the list be unordered.
    """

    def __init__(self, context: ContextLike, var_map: VariableMap = None):
        self.context_args = []
        if isinstance(context, ScopeList):
            var_map = self._parse_context_from_scope_list(context, var_map)
        elif isinstance(context, dict):
            self.load_context_from_dict(context)
        elif (isinstance(context, list) and len(context) > 1) or (isinstance(context, tuple) and len(context) > 1):
            self.load_context_from_args(context)
        elif (isinstance(context, list) or isinstance(context, tuple)) and len(context) == 1 and isinstance(context[0], str):
            self.load_context_from_args(context[0].split())
        elif isinstance(context, tuple) or isinstance(context, list):
            pass
        else:
            raise AttributeError('Context must be initialised with a list of argument or dictionary object')
        self.var_loaded = False
        self.events_loaded = False
        if var_map:
            self.load_variables(var_map)
        self.event_variables = {}

    def __getitem__(self, item):
        if hasattr(self, item):
            return getattr(self, item)
        return None

    @property
    def available_scopes(self) -> ScopeList:
        """
        Property that returns a ScopeList of all available scopes in the context object.
        e.g.

        if s1 = EXG and s2 = 5m, then the available scopes
        will be ScopeList([Scope('SCENARIO', 'EXG'), Scope('SCENARIO', '5m')])
        """

        avail_scopes = ScopeList()
        for a in ['S', 'E']:
            avail_scopes.extend([Scope(a, s) for s in self._create_list(a)])
        for k in self.event_variables.keys():
            avail_scopes.append(Scope('EVENT VARIABLE', k))
        for k, v in self.__dict__.items():
            if re.findall(r'^[_a-z]', k):
                continue
            if re.findall(r'^[es]\d?$', k, flags=re.IGNORECASE) or k == 'event_variables':
                continue
            if isinstance(v, list):
                for x in v:
                    avail_scopes.append(Scope('VARIABLE', x, var='<<{0}>>'.format(k)))
            else:
                avail_scopes.append(Scope('VARIABLE', v, var='<<{0}>>'.format(k)))
        return avail_scopes

    def is_empty(self) -> bool:
        """
        Returns whether the context object is empty. Will not count variables as if they are not
        controlled by scenarios/events, then they don't need context to be resolved.
        """

        return len(self._create_list('S')) == 0 and len(self._create_list('E')) == 0

    def load_context_from_dict(self, context: dict) -> None:
        """Public function that will load the context object from a dictionary."""
        self.context_args = sum([[k, v] for k, v in context.items()], [])
        for i, key in enumerate(self.context_args[::2]):
            if key and key[0] != '-':
                self.context_args[i * 2] = '-{0}'.format(key)
        context = self._convert_to_lower_keys(context)
        self._parse_context_from_dict(context, 's')
        self._parse_context_from_dict(context, 'e')

    def load_context_from_args(self, args) -> None:
        """Public function that will load the context from a list or args."""
        context = {}
        self.context_args = args[:]
        self._parse_context_from_args(args, 's', context)
        self._parse_context_from_args(args, 'e', context)
        self.load_context_from_dict(context)

    def load_variables(self, var_map: VariableMap) -> None:
        """
        Public function that will load variables into the context object. The variable map should be a dictionary
        of the variable names and the variable values.
        """

        if var_map is None:
            return
        for key, value in var_map.items():
            key = key.upper()
            setattr(self, key, value)
        self.var_loaded = True

    def load_events(self, event_db: EventDatabase) -> None:
        """Public function that will load an event database into the context object."""
        if not event_db:
            return
        self._event_db = event_db
        event_list = self._create_list('E')
        for e in event_list:
            event = event_db.get(e)
            if event is None:
                raise KeyError('Event {0} not found'.format(e))
            self.event_variables[event.variable.upper()] = event.value
        self.events_loaded = True

    def in_context_by_scope(self, req_scope: ScopeList) -> bool:
        """
        Mutually exclusive will treat IF/ELSE IF blocks as mutually exclusive.

        i.e. in the example below, D01 and D02 are mutually exclusive, if D01 and D02 are both specified
        only the D01 block will be run. Setting mutually_excl to False will trigger both blocks, i.e. maybe the user
        wants to copy all input files for both scenarios. ELSE blocks will always be treated as mutually exclusive.

        IF Scenario == D01
            ! do something
        Else IF Scenario == D02
            ! do something else
        END IF

        """

        if req_scope is None or Scope('GLOBAL') in req_scope:
            return True

        for s in req_scope:
            if not s.resolvable():  # "Start 1D Domain" or "Define Output Zone" etc.
                continue
            if s not in self.available_scopes and not s.is_neg():
                return False
            elif s in self.available_scopes and s.is_neg():
                return False
            elif isinstance(s, VariableScope) and '<<' in s.name:
                return False

        return True

    def translate(self, item: any) -> any:
        """
        Translates input string into a resolved string.
        It does this by replacing all variables with their values.
        """

        if not isinstance(item, str) and not isinstance(item, TuflowPath):
            return item
        name_ = str(item)
        name_ = replace_exact_names('<<.+?>>', self.__dict__, name_)
        for key, value in self.event_variables.items():
            name_ = replace_exact_names(re.escape(key), self.event_variables, name_)
        return name_

    def translate_result_name(self, tcf_name: str) -> str:
        """Translates the TCF file name into a string replacing all variables with their values."""
        a = [x.strip('-') for x in self.context_args]
        a_upper = [x.upper() for x in a]
        a_used = []
        name_ = TuflowPath(tcf_name).stem
        for match in re.findall(r'~[SsEe]\d?~', tcf_name):
            var = match.upper()[1:-1]
            if var in a_upper:
                i = a_upper.index(var)
            elif var == 'E' and 'E1' in a_upper:
                i = a_upper.index('E1')
            elif var == 'E1' and 'E' in a_upper:
                i = a_upper.index('E')
            elif var == 'S' and 'S1' in a_upper:
                a_upper.index('S1')
            elif var == 'S1' and 'S' in a_upper:
                i = a_upper.index('S')
            else:
                continue
            val = self.context_args[i + 1]
            name_ = name_.replace(match, val)
            if var not in a_used:
                a_used.append(var)
        for var in reversed(a_used):
            i = a_upper.index(var)
            a.pop(i + 1)
            a.pop(i)
        e_list = []
        s_list = []
        for i, var in enumerate(a[::2]):
            val = a[i * 2 + 1]
            if var.upper()[0] == 'E':
                e_list.append(val)
            elif var.upper()[0] == 'S':
                s_list.append(val)
        for i, val in enumerate(e_list):
            if i == 0:
                name_ = f'{name_}_{val}'
            else:
                name_ = f'{name_}+{val}'
        for i, val in enumerate(s_list):
            if i == 0:
                name_ = f'{name_}_{val}'
            else:
                name_ = f'{name_}+{val}'
        return name_

    def is_resolved(self, item: any) -> bool:
        """
        Checks whether item has been resolved (to the best of its ability). Event variable names are custom and
        is not always possible to know whether they have been resolved or not.
        """

        if not isinstance(item, str) and not isinstance(item, TuflowPath):
            return True
        if re.findall(r'<<.+?>>', str(item)):
            logger.warning('Item could not be resolved by context: {}'.format(str(item)))
            return False
        for key, value in self.event_variables.items():
            if key in str(item):
                logger.warning('Item could not be resolved by context: {}'.format(str(item)))
                return False

        return True

    def _parse_context_from_scope_list(self, scope_list: ScopeList, var_map: VariableMap = None) -> dict:
        """
        Initialises the context object from a scope list. This will pretty much always
        create an unordered list.
        """

        d = {'s': [], 'e': []}
        if not var_map:
            var_map = {}
        for scope in scope_list:
            if scope == Scope('SCENARIO'):
                if isinstance(scope.name, list):
                    for n in scope.name:
                        d['s'].append(n)
                elif scope.name:
                    d['s'].append(scope.name)
            elif scope == Scope('EVENT'):
                if isinstance(scope.name, list):
                    for n in scope.name:
                        d['e'].append(n)
                elif scope.name:
                    d['e'].append(scope.name)
            elif scope == Scope('VARIABLE'):
                if isinstance(scope.name, list):
                    if scope._var is not None:
                        var = scope._var.strip('<>')
                    else:
                        logger.error('Context could not be initialised; variable scope must have a variable name')
                        raise ValueError('Variable scope must have a variable name')
                    if var in var_map:
                        if not isinstance(var_map[var], list):
                            var_map[var] = [var_map[var]]
                        var_map[var].extend(scope.name)
                    else:
                        var_map[var] = scope.name[0]

        self.load_context_from_dict(d)

        return var_map

    def _parse_context_from_dict(self, context: dict, identifier: str) -> None:
        """
        Initialises the context object from a dictionary.
        e.g. {'s1': 'EXG', 's2': '5m', 'e1': '100y'}
        """

        s = context.get(identifier, None)
        s1 = '{0}1'.format(identifier)
        if s and context.get(s1, None):
            raise ValueError('Context cannot have both {0} and {1}'.format(s, s1))
        if s and isinstance(s, list) and len(s) > 1:
            setattr(self, identifier.upper(), s)
        elif s or context.get(s1, None):
            for i in range(1, 10):
                if i == 1 and s:
                    key = s1
                    if isinstance(s, list):
                        value = s[0]
                    else:
                        value = s
                else:
                    key = '{0}{1}'.format(identifier, i)
                    value = context.get(key, None)
                if value:
                    if hasattr(self, key.upper()):
                        logger.error('Context could not be initialised; context already has attribute {}'.format(key))
                        raise ValueError('Context already has attribute {0}'.format(key))
                    setattr(self, key.upper(), value)

    def _parse_context_from_args(self, args: list[str], identifier: str,
                                 context: dict[str, Union[str, list[str]]]) -> None:
        """
        Initialises the context object from a list of arguments for a specific identifer (e.g. 's' or 'e').
        This routine will return a dictionary of the variable name and the variable value.
        e.g. ['s1', 'EXG', 's2', '5m'] = {'s1': 'EXG', 's2': '5m'}

        This routine will consider both ordered and unordered lists:
        e.g. ['s', 'EXG', 's', '5m'] = {'s': ['EXG', '5m']}
        """
        while args:
            a = args[0]
            args = args[1:]
            if re.findall(r'-?[{0}{1}]\d?'.format(identifier.upper(), identifier.lower()), a):
                if a.lower() in ['-{0}'.format(identifier), identifier]:
                    if context.get(identifier) and not isinstance(context[identifier], list):
                        context[identifier] = [context[identifier], args[0]]
                    elif context.get(identifier):
                        context[identifier].append(args[0])
                    else:
                        context[identifier] = args[0]
                else:
                    if a.upper().strip('-') in [x.upper() for x in context.keys()]:
                        raise ValueError('Duplicate context values for {0}'.format(a.lower().strip('-')))
                    context[a.lower().strip('-')] = args[0]
                args = args[1:]
            else:
                args = args[1:]
        if context.get(identifier) and not isinstance(context[identifier], list):
            context['{0}1'.format(identifier)] = context[identifier]
            del context[identifier]

    def _convert_to_lower_keys(self, d: dict[str, any]) -> dict[str, any]:
        d_out = {}
        for k, v in d.items():
            if re.findall(r'^[EeSs]\d?$', k):
                d_out[k.lower()] = v
            else:
                d_out[k] = v
        return d_out

    def _create_list(self, identifier: str) -> list[str]:
        """
        Creates a list of all the available scope names given an identifier (e.g. 's', 'e')

        This will create an unordered list, so s1, s2 names will be return as ['5m', 'EXG'].
        """

        if not hasattr(self, identifier):
            list_ = [getattr(self, '{0}{1}'.format(identifier, i)) for i in range(9) if
                      hasattr(self, '{0}{1}'.format(identifier, i)) and getattr(self, '{0}{1}'.format(identifier, i))]
        else:
            list_ = getattr(self, identifier)
        return list_


