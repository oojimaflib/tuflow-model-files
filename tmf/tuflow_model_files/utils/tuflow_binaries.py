from typing import TYPE_CHECKING
import json
from pathlib import Path

from .settings import get_cache_dir

if TYPE_CHECKING:
    from ..dataclasses.types import PathLike

from ..utils import logging as tmf_logging
logger = tmf_logging.get_tmf_logger()


class TuflowBinaries:

    def __init__(self):
        self._tuflow_version_json = self.tuflow_version_json()
        self.version2bin = self.load_tuflow_version_cache()

    def __repr__(self):
        return '<TuflowBinaries>'

    def __contains__(self, item):
        return item in self.version2bin

    def __getitem__(self, item):
        return self.version2bin[item]

    @staticmethod
    def tuflow_version_json() -> Path:
        """Returns the path to the JSON file containing stored TUFLOW version info."""
        return Path(get_cache_dir()) / 'tuflow_versions.json'

    def load_tuflow_version_cache(self):
        """Load the TuflowVersions object from the JSON file."""
        if self._tuflow_version_json.exists():
            with self._tuflow_version_json.open() as fo:
                return json.load(fo)
        return {}

    def save_tuflow_version_cache(self) -> None:
        """Saves the tuflow versions to the cache (JSON file)."""
        if not Path(get_cache_dir()).exists():
            Path(get_cache_dir()).mkdir(parents=True)
        with self._tuflow_version_json.open('w') as fo:
            json.dump(self.version2bin, fo, indent=4)


tuflow_binaries = TuflowBinaries()


def register_tuflow_binary(version_name: str, version_path: 'PathLike') -> None:
    """Adds a tuflow binary to the cache."""
    tuflow_binaries.version2bin[version_name] = str(version_path)
    tuflow_binaries.save_tuflow_version_cache()
    logger.info('New TUFLOW binary registered: {} - {}'.format(version_name, version_path))
