import json
import os
import re
import typing

from ...convert_tuflow_model_gis_format.conv_tf_gis_format.helpers.command import Command, EventCommand
from ...convert_tuflow_model_gis_format.conv_tf_gis_format.helpers.parser import get_commands
from .gis import ogr_format, get_database_name, ogr_iter_geom, gdal_format
from .settings import MinorConvertException
from ..dataclasses.file import TuflowPath

from ..utils import logging as tmf_logging
logger = tmf_logging.get_tmf_logger()


def build_tuflow_command_string(input_text: typing.Union[str, list[str]], command: Command) -> str:
    """
    Build a TUFLOW command string from input_text and a reference command object.

    The left side of the command will try and be guessed from the input_text if it is not provided.
    """
    cmd = None
    if not input_text.strip():
        return input_text
    if '!' in input_text and not input_text.split('!')[0].strip():
        return input_text

    if isinstance(input_text, list) or (' == ' not in input_text and not command.auto_estry_old):
        if isinstance(input_text, list):
            text = input_text[0]
        else:
            text = input_text

        type_ = 'setting'
        if TuflowPath(text).is_file():
            if re.findall('^[012]d_', TuflowPath(text).lyrname, re.IGNORECASE):
                type_ = 'gis'
            elif TuflowPath(text).suffix.lower() in ['.asc', '.flt', '.tif', '.gpkg' '.nc']:
                type_ = 'grid'
            elif TuflowPath(text).suffix.lower() in ['.12da', '.xml']:
                type_ = 'tin'
        if isinstance(input_text, list) and input_text:
            left = guess_command_from_text(TuflowPath(input_text[0]).name, type_)
        else:
            left = guess_command_from_text(TuflowPath(input_text).name, type_)
        if not left:
            logger.error('Could not guess command from text: {}'.format(text))
            raise ValueError('Could not guess command from text')
    elif not command.auto_estry_old:
        cmd = Command(input_text, command.settings)
        left, right = cmd.command_orig, cmd.value_orig
        input_text = [x.strip() for x in right.split('|')]
        if len(input_text) == 1:
            input_text = input_text[0]
    else:
        left = input_text.strip()
        input_text = ''

    if isinstance(input_text, list) or TuflowPath(input_text).is_file():
        settings = command.settings
        input_text = concat_command_values(settings.control_file, input_text, settings.spatial_database)
    input_text = f'{left} == {input_text}'
    if cmd:
        input_text = cmd.re_add_comments(input_text, rel_gap=True)

    if input_text and input_text[-1] != '\n':
        input_text = f'{input_text}\n'

    return input_text


def guess_command_from_text(text: str, type_: str = '') -> str:
    json_file = TuflowPath(__file__).parent.parent.parent / 'data' / 'command_db.json'
    with json_file.open() as f:
        data = json.load(f)
        if type_:
            data = data.get(type_)
            if not data:
                return ''
        if isinstance(data, str):
            return data
        for key, value in data.items():
            if key.lower() in text.lower():
                return value
    return ''


def concat_command_values(control_file: 'TuflowPath', values: list[str], spatial_database: 'TuflowPath') -> str:
    if not isinstance(values, list):
        values = [values]
    if not values:
        return ''

    tf_cmd = TuflowCommand(control_file, values[0], spatial_database)
    if not tf_cmd:
        return ' | '.join(values)
    if len(values) > 1:
        for value in values[1:]:
            tf_cmd.append(control_file, value, spatial_database)
    return tf_cmd.command_right


class TuflowCommand:

    def __new__(cls, control_file, fpath, spatial_database):
        fpath = TuflowPath(fpath)
        try:
            fmt = ogr_format(fpath)
        except MinorConvertException:
            try:
                fmt = gdal_format(fpath)
            except MinorConvertException:
                if fpath.is_file():
                    fmt = 'TIN'
                    logger.warning('MinorConvertException: Format assumed to be "TIN"')
                else:
                    fmt = None
                    logger.warning('MinorConvertException: Format unknown')
        except TypeError:
            fmt = 'NUMBER'
        if fmt == 'GPKG':
            cls = TuflowCommandGPKG
        elif fmt == 'Esri Shapefile':
            cls = TuflowCommandSHP
        elif fmt == 'Mapinfo File':
            cls = TuflowCommandMapinfo
        elif fmt == 'NUMBER':
            cls = TuflowCommandNumber
        elif fmt in ['GTiff', 'AAIGrid', 'EHdr', 'netCDF']:
            cls = TuflowCommandRaster
        elif fmt == 'TIN':
            cls = TuflowCommandTin
        else:
            return
        self = super().__new__(cls)
        self._init(control_file, fpath, spatial_database)
        return self

    def __repr__(self):
        return '<{0} {1}>'.format(self.__class__.__name__, self.name)

    def _init(self, control_file, ds, spatial_database) -> None:
        self.valid = False
        self.ds = ds
        self.file, self.name = get_database_name(self.ds)
        self.file = TuflowPath(self.file)
        self.cf = control_file
        self.spatial_database = spatial_database
        self.valid = self.cf is not None

    @property
    def command_right(self) -> str:
        if self.valid:
            relpath = os.path.relpath(self.file, str(self.cf.parent))
            return '{0}'.format(relpath)
        return ''

    def append(self, control_file, ds, spatial_database) -> bool:
        return False


class TuflowCommandMapinfo(TuflowCommand):
    pass


class TuflowCommandRaster(TuflowCommand):
    pass


class TuflowCommandTin(TuflowCommand):
    pass


class TuflowCommandNumber(TuflowCommand):

    def __repr__(self):
        return '<{0} {1}>'.format(self.__class__.__name__, self.ds)

    def _init(self, control_file, ds, spatial_database) -> None:
        self.ds = ds
        self.valid = True

    @property
    def command_right(self) -> str:
        return str(self.ds)


class TuflowCommandSHP(TuflowCommand):

    def _init(self, control_file, ds, spatial_database) -> None:
        super()._init(control_file, ds, spatial_database)
        self._in_right = False
        self.commands = [self]

    def appendable(self, command: 'TuflowCommand') -> bool:
        return True  # leave it up to user to manage when commands are concatenated

    def append(self, control_file, ds, spatial_database) -> bool:
        command = TuflowCommand(control_file, ds, spatial_database)
        if self.valid and command.valid and self.appendable(command):
            if len(self.commands) > 1 and type(command) != type(self):
                for c in self.commands:
                    if type(c) == type(command):
                        c.commands.append(command)
                        return True
            self.commands.append(command)
            return True
        return False

    def command_iter(self) -> typing.Generator['TuflowCommand', None, None]:
        for command in self.commands:
            yield command

    @property
    def command_right(self) -> str:
        if self.valid:
            if len(self.commands) == 1 or self._in_right:
                return super().command_right
            self._in_right = True
            rhs = ' | '.join([x.command_right for x in self.command_iter()])
            self._in_right = False
            return rhs
        return ''


class TuflowCommandGPKG(TuflowCommandSHP):

    def _init(self, control_file, ds, spatial_database) -> None:
        super()._init(control_file, ds, spatial_database)
        self.type = 'name' if self.file == spatial_database else 'path'

    @property
    def command_right(self) -> str:
        if self.valid and self.type == 'name':
            return ' | '.join([x.name for x in self.command_iter()])
        if self.valid and len(self.commands) == 1 and self.file.stem.lower() == self.name.lower():
            return super().command_right
        dbs = [x for x, y in self.db_iter()]
        if self.valid and len(dbs) == 1:
            return '{0} >> {1}'.format(TuflowCommand.command_right.fget(self), ' && '.join([x.name for x in self.command_iter()]))
        elif self.valid:
            commands = []
            for db, command in self.db_iter():
                relpath = os.path.relpath(db, str(command.cf.parent))
                if isinstance(command, TuflowCommandGPKG):
                    c = '{0} >> {1}'.format(relpath, ' && '.join(self.names(db)))
                else:
                    c = TuflowCommandSHP.command_right.fget(command)
                commands.append(c)
            return ' | '.join(commands)
        return ''

    def db_iter(self):
        db = []
        for command in self.command_iter():
            if command.file not in db:
                db.append(command.file)
                yield command.file, command

    def names(self, db) -> typing.List[str]:
        return [x.name for x in self.command_iter() if x.file == db]
