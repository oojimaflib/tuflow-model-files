from .cf.tcf import TCF
from .cf.tgc import TGC
from .cf.tbc import TBC
from .cf.ecf import ECF
from .cf.tef import TEF
from .cf.qcf import QCF
from .cf.adcf import ADCF
from .cf.tesf import TESF
from .cf.trd import TRD
from .cf.trfc import TRFC
from .cf.toc import TOC
from .dataclasses.scope import Scope
from .utils.tuflow_binaries import tuflow_binaries, register_tuflow_binary


# Setup default logging handlers for the API.
# If there is an existing logging configuration, no logger is configured, the
# calling code will override with its preferences
import logging
import sys


# Retrieve the "tmf" logger and check whether there are any existing logging handlers.
# If not, setup the default console StreamHandler with level set to WARNING
tmf_logger = logging.getLogger('tmf')
tmf_handler = None
if tmf_logger.hasHandlers() is False:
    tmf_handler = logging.StreamHandler()
    tmf_handler.setFormatter(logging.Formatter("%(asctime)s %(module)-30s line:%(lineno)-4d %(levelname)-8s %(message)s"))
    tmf_handler.setStream(sys.stdout)
    tmf_logger.setLevel(logging.WARNING)
    tmf_logger.addHandler(tmf_handler)
    tmf_logger.warning("Add a console logging handler to tmf logger. Use log_level keyword to change level")


def disable_log_handler(log_name: str = 'tmf', handler: logging.Handler = tmf_handler) -> None:
    """Disable default log handler added to tmf_logger when the module is imported.
    
    Default kwargs are placeholders for possible future use, they are not used and
    have no impact on the behaviour of the function at the moment.
    """
    if tmf_logger.hasHandlers() and tmf_handler is not None:
        tmf_logger.removeHandler(tmf_handler)